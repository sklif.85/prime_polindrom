import sys
from math import sqrt
import time
import datetime

def check_prime(num):
    for i in range(2, int(sqrt(num)) + 1):
        if num % i == 0:
            return False
    return True


def get_palindrom():
    start = time.time()
    s = [4, 5, 6]
    # for num in sys.stdin:
    for num in s:
        brk = False
        num1 = (10 ** int(num)) - 1
        for i in range(num1):
            if brk == True:
                break
            if check_prime(num1 - i):
                prime1 = num1 - i
                num2 = prime1
                for j in range(num2):
                    if check_prime(num2 - j):
                        prime2 = num2 - j
                        res = str(int(prime1) * int(prime2))
                        if res == res[::-1]:
                            print(res)
                            end = time.time()
                            print("Running time -> TEST[ " + str(num) + " ]" + str(end - start))
                            brk = True
                            break


def main():
    print("Get me a number: ")
    get_palindrom()


if __name__ == '__main__':
    main()