import sys
from math import sqrt
import time
import datetime
import threading

def check_prime(num):
    for i in range(2, int(sqrt(num)) + 1):
        if num % i == 0:
            return False
    return True

def find_next_prime(prime1, dig, time_start):
    # global polinrom
    i = 0
    while len(str(prime1 - i)) == dig:
        if check_prime(prime1 - i):
            res = str(int(prime1) * int(prime1 - i))
            if res == res[::-1]:
                print(res)
                time_end = time.time()
                print("Running time -> TEST [ " + str(dig) + " ] -> " + str(time_end - time_start))
                return True
        i += 1

def get_palindrom():
    print("Get me a number: ")
    stdin = [3, 4, 5]
    print(stdin)
    for dig in stdin:
    # for dig in sys.stdin:
        time_start = time.time()
        num = (10 ** int(dig)) - 1
        while int(num) >= len(str(dig)):
            if check_prime(int(num)):
                if find_next_prime(num, dig, time_start):
                    break
            num -= 1

def main():
    get_palindrom()

if __name__ == '__main__':
    main()